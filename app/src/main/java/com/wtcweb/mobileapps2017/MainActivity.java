package com.wtcweb.mobileapps2017;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private ListView listView;
    private ArrayList<String> activityNames;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get a handle on the listView that's in the layout file for this activity
        listView = (ListView)findViewById(R.id.mainListView);

        // get an array list of all the activities that are included in this app
        // (see the code in the getActivities() method to see how I did it)
        activityNames = getActivities();

        // populate the listView with the activity names (we'll learn more about this in chapter 6)
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, activityNames);
        listView.setAdapter(adapter);

        // set up the listView so that it responds to clicks
        // (ex: when a user presses an item in the list)
        // this code is a littl funky, it uses an anonymous inner class that
        // has a method called onItemClick(), this method will get called when an
        // item in the list is clicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String selectedActivityName = activityNames.get(position);

                /*
                Toast.makeText(getApplicationContext(), selectedActivityName, Toast.LENGTH_LONG).show();
                // note that in the above line, the first param cannot be 'this'
                // because we are inside an anonymous inner class, so 'this' refers to
                // the anonymous innter class, not the MainActivity class
                // so we use getApplicationContext() method instead
                */

                // Use an intent to launch the selectedActivity
                try {
                    Intent i = new Intent(getApplicationContext(), Class.forName(selectedActivityName));
                    startActivity(i);
                } catch (ClassNotFoundException e) {
                    Log.d(TAG, e.getMessage());
                }


            }
        });
    } // end of onCreate()

    private ArrayList<String> getActivities(){

        ArrayList<String>activityNames = new ArrayList<String>();

        try {
            String packageName = this.getPackageName();
            PackageInfo pi = getPackageManager().getPackageInfo(
                    packageName, PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo>activities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for(ActivityInfo ai : activities){
                activityNames.add(ai.name);
            }

            //TESTING...
            //Log.d(TAG, "Show Activities...");
            //Log.d(TAG, activityNames.toString());
            //Toast.makeText(this, activityNames.toString(),Toast.LENGTH_LONG).show();

        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage());
        }

        return activityNames;
    }
}

